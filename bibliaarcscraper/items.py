# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ChapterItem(scrapy.Item):
    href = scrapy.Field()
    title = scrapy.Field()

class VerseItem(scrapy.Item):
    _id = scrapy.Field()
    text = scrapy.Field()
    header = scrapy.Field()
    subheader = scrapy.Field()
    book = scrapy.Field()
    chapter = scrapy.Field(dblite='integer')
    verse = scrapy.Field(dblite='integer')


class BookItem(scrapy.Item):
    _id = scrapy.Field()
    name = scrapy.Field()
    abbreviation = scrapy.Field()
    testament = scrapy.Field()
