# -*- coding: utf-8 -*-

# Scrapy settings for bibliaarcscraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'bibliaarcscraper'

SPIDER_MODULES = ['bibliaarcscraper.spiders']
NEWSPIDER_MODULE = 'bibliaarcscraper.spiders'

ITEM_PIPELINES = {
    'bibliaarcscraper.pipelines.SqliteItemsPipeline':1,
    #'bibliaarcscraper.pipelines.JsonWithEncodingPipeline':2,
}

#LOG_FILE = 'error.log'
#LOG_LEVEL = 'WARNING'

#AUTOTHROTTLE_ENABLED = True

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'bibliaarcscraper (+http://www.yourdomain.com)'
