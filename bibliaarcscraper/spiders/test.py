# -*- coding: utf-8 -*-
import urlparse

import scrapy
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request, Response
from scrapy.selector import Selector

from bibliaarcscraper.items import ChapterItem, VerseItem
from bibliaarcscraper.spiders.verses import parse_bible_chapter

# scrapy crawl test -o verses.json -t json
class TestSpider(CrawlSpider):
    name = "test"
    allowed_domains = ["www.biblegateway.com"]
    start_urls = (
        #'https://www.biblegateway.com/passage/?search=Salmos+150&version=ARC',
        'https://www.biblegateway.com/passage/?search=Mateus+4&version=ARC',
        #'https://www.biblegateway.com/passage/?search=Mateus+6&version=ARC',
        #'https://www.biblegateway.com/passage/?search=G%C3%AAnesis+1&version=ARC',
    )
    item_class = VerseItem

    def parse(self, response):
        return parse_bible_chapter(self, response)
