# -*- coding: utf-8 -*-
import urlparse

import scrapy
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request, Response
from scrapy.selector import Selector

from bibliaarcscraper.items import ChapterItem, VerseItem

def parse_bible_chapter(spider, response):
    sel = Selector(response)
    verses = sel.xpath('//div[contains(@class, "version-ARC")]/p/span')
    headers = sel.xpath('//div[contains(@class, "version-ARC")]/h3')
    subheaders = sel.xpath('//div[contains(@class, "version-ARC")]/h4')

    for verse in verses:
        item = VerseItem()
        #if verse.xpath('span[contains(@class, "woj")]/text()').extract():
        #    item['text'] = "".join(verse.xpath('span[contains(@class, "woj")]/node()[not(self::sup or self::span[@class="chapternum"])]').extract()).replace(' class="small-caps divine-name"', '').replace(' class="added"', '')
        #else:
        #    item['text'] = "".join(verse.xpath('node()[not(self::sup or self::span[@class="chapternum"])]').extract()).replace(' class="small-caps divine-name"', '').replace(' class="added"', '')
        item_xpath = 'node()[not(self::sup or self::span[@class="chapternum"])]'
        item['text'] = "".join(verse.xpath(item_xpath).extract())
        item['text'] = item['text'].replace(' class="small-caps divine-name"', '')
        item['text'] = item['text'].replace(' class="added"', '')

        verse_class = verse.xpath('@class').extract()[0]
        verse_index = verse_class.replace("text", "").strip()
        item['book'], item['chapter'], item['verse'] = verse_index.split('-')
        item['book'] = item['book'].lower()

        verse_headers = [s for s in headers if s.xpath('span/@class').extract()[0] == verse_class]
        if verse_headers:
            item['header'] = "".join(verse_headers[0].xpath('span/node()').extract()).replace(' class="small-caps divine-name"', '')
        header_subs = [s for s in subheaders if s.xpath('span/@class').extract()[0] == verse_class]
        if header_subs:
            item['subheader'] = "".join(header_subs[0].xpath('span/node()').extract()).replace(' class="small-caps divine-name"', '')
        yield item

# scrapy crawl verses -o verses.json -t json
class VersesSpider(CrawlSpider):
    name = "verses"
    allowed_domains = ["www.biblegateway.com"]
    start_urls = (
        'https://www.biblegateway.com/versions/Almeida-Revista-e-Corrigida-2009-ARC/#booklist/',
    )
    item_class = VerseItem

    def parse(self, response):
        sel = Selector(response)
        items = sel.xpath('//td[contains(@class, "chapters")]/a')
        chapters = []
        base_url = 'https://www.biblegateway.com'
        
        for item in items:
            chapter = ChapterItem()
            chapter['title'] = item.xpath('@title').extract()[0]
            chapter['href'] = urlparse.urljoin(base_url, item.xpath('@href').extract()[0])
            url = urlparse.urljoin(base_url, item.xpath('@href').extract()[0])
            yield scrapy.Request(url, callback = self.parse_chapter)
    
    def parse_chapter(self, response):
        return parse_bible_chapter(self, response)
        