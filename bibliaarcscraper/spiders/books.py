# -*- coding: utf-8 -*-
import urlparse

import scrapy
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request, Response
from scrapy.selector import Selector

from bibliaarcscraper.items import ChapterItem, VerseItem, BookItem

# scrapy crawl books -o books.json -t json
class BooksSpider(CrawlSpider):
    name = "books"
    allowed_domains = ["www.biblegateway.com"]
    start_urls = (
        'https://www.biblegateway.com/versions/Almeida-Revista-e-Corrigida-2009-ARC/',
    )
    item_class = BookItem

    def parse(self, response):
        sel = Selector(response)
        table_rows = sel.xpath('//table[contains(@class, "chapterlinks")]//tr')
        
        for row in table_rows:
            book = BookItem()
            row_class = row.xpath('@class').extract()[0]
            book['name'] = row.xpath('td[contains(@class, "book-name")]/text()').extract()[0]
            abbrev, testament = row_class.split()
            book['abbreviation'] = abbrev.replace('-list', '')
            book['testament'] = testament.replace('-book', '')
            yield book
        