# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os, json, codecs

from scrapy.exceptions import DropItem
from bibliaarcscraper.items import VerseItem, BookItem
import dblite

curdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)

class SqliteItemsPipeline(object):
    def __init__(self):
        self.ds = None

    def open_spider(self, spider):
        db_file = 'sqlite://%s:%s' % (os.path.join(curdir, 'biblia_arc.db'), spider.name)
        self.ds = dblite.open(spider.item_class, db_file, autocommit=True)

    def close_spider(self, spider):
        self.ds.commit()
        self.ds.close()

    def process_item(self, item, spider):
        if isinstance(item, VerseItem) or isinstance(item, BookItem):
            try:
                self.ds.put(item)
            except dblite.DuplicateItem:
                raise DropItem("Duplicate item found: %s" % item)
        else:
            raise DropItem("Unknown item type, %s" % type(item))
        return item
    
class JsonWithEncodingPipeline(object):
    def __init__(self):
        self.file = codecs.open('verses.json', 'w', encoding='utf-8')

    def process_item(self, item, spider):
        line = json.dumps(dict(item), ensure_ascii=False) + "\n"
        self.file.write(line)
        return item

    def spider_closed(self, spider):
        self.file.close()